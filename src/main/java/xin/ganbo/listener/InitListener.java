package xin.ganbo.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;
import xin.ganbo.entity.User;
import xin.ganbo.utils.RedisCacheManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/2/7.
 */

@WebListener
public class InitListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        try {
            RedisCacheManager redisCacheManager = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext()).getBean(RedisCacheManager.class);
            List<User> users = new ArrayList();

            User u1 = new User(1L, "Bruce", "123", new Date());
            User u2 = new User(2L, "Nick", "666", new Date());
            User u3 = new User(3L, "James", "777", new Date());

            users.add(u1);
            users.add(u2);
            users.add(u3);

            redisCacheManager.lSet("users", users);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("==============创建");
    }

    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("销毁容器===========");
    }

}
